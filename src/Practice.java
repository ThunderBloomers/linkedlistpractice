/**
 * 
 */

/**
 * @author carro
 *
 */
public class Practice {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		CustomLinkedList list = new CustomLinkedList();
		
		list.addNode("Turkey");
		list.addNode("Straw");
		list.addNode("Farmer");
		list.addNode("Fork");
		
		list.displayList();

		/*
		list.removeNode("Straw");
		list.displayList();
		
		list.removeNode("Pants");
		
		list.removeNode("Turkey");
		list.displayList();
		list.removeNode("Farmer");
		list.displayList();
		list.removeNode("Fork");
		list.displayList();
		*/
		
		//list.splitAndRestart(1);
		list.splitAndRestart(3);
		list.displayList();
	}

}
