
public class Node {
	
	Node next;
	String content;

	public Node(String content){
		this.content = content;
	}
	
	public String toString(){
		return "Node: " + this.content;
	}

}
