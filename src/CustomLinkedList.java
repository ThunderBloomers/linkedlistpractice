

public class CustomLinkedList {
		Node head;
		
		public void addNode(String content){
			
			Node newNode = new Node(content);
			
			if (this.head == null){
				this.head = newNode;
			
			}else{
				newNode.next = this.head;
				this.head = newNode;
			}
			
			
		}
		
		public void displayList(){
			
			System.out.println("\n");
			
			if(this.head!=null){
				
				Node currentNode = head;
				
				while(currentNode != null){
					System.out.print(currentNode + ", ");
					currentNode = currentNode.next;
				}
				
			}else{
				System.out.println("List is Empty.");
			}
			
			System.out.println("\n");
			
		}
		
		public void removeNode(String contentToRemove){
			
			if (this.head != null){
				
				Node currentNode = this.head;
				String comparison = currentNode.content;
				
				if (comparison.equals(contentToRemove)){
					if (currentNode.next != null){
						this.head = currentNode.next;
					}else{
						this.head = null;
					}
				}else{
					while(currentNode.next != null){
						
						comparison = currentNode.next.content;
						
						if(comparison.equals(contentToRemove)){
							currentNode.next = currentNode.next.next;
							return;
						}else{
							currentNode = currentNode.next;
						}
						
					}
					System.out.println(contentToRemove + " not found.");
				}
			}else{
				
				System.out.println("Empty list");
			}
			
			
		}
		
		public int currentListLength(){
			
			int count = 0;
			
			Node currentNode = this.head;
			
			while (currentNode != null){
				count++;
				currentNode = currentNode.next;
			}
			
			return count;
		}
		
		public void splitAndRestart(int index){
			
			int length = currentListLength();
			Node currentNode = this.head;
			Node previousNode = this.head;
			
			if(index > length){
				System.out.println("cannot remove at this index");
				return;
			}
			
			if(index <= 0){
				System.out.println("This doesn't make sense");
				return;
			}
			
			if(index == 1){
				currentNode = previousNode.next;
				Node tempHead = this.head;
				Node lastNode = getLastNode();
				this.head = currentNode;
				lastNode.next = tempHead;
				previousNode.next = null;
			}
			
			for (int i=0; i < index-1; i++){
				previousNode = currentNode;
				currentNode = currentNode.next;
			}
			
			Node tempHead = this.head;
			Node lastNode = getLastNode();
			this.head = currentNode;
			lastNode.next = tempHead;
			previousNode.next = null;
			
		}
		
		public Node getLastNode(){
			Node currentNode = this.head;
			
			while(currentNode.next != null){
				currentNode = currentNode.next;
			}
			
			return currentNode;
		}
}
